﻿using System;
using TestMLML.Model;

namespace MLCoreTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = new ModelInput();
            input.SentimentText = "You are nice";

            ModelOutput result = ConsumeModel.Predict(input);

            Console.WriteLine($"Text: {input.SentimentText}\nIs Toxic: {result.Prediction}");
            Console.ReadKey();
        }
    }
}
