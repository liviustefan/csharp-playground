﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMLML.Model;

namespace TestML
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = new ModelInput();
            input.SentimentText = "That is rude";

            ModelOutput result = ConsumeModel.Predict(input);

            Console.WriteLine($"Text: {input.SentimentText}\nIs Toxic: {result.Prediction}");
            Console.ReadKey();
        }
    }
}
