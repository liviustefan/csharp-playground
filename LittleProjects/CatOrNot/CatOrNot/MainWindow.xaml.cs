﻿using CatOrNotML.Model;
using Microsoft.Win32;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media.Imaging;

namespace CatOrNot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ModelInput Input { get; set; }

        private string ImagePath { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            // Add input data
            this.Input = new ModelInput();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog opg = new OpenFileDialog();

            if (opg.ShowDialog() == true)
            {
                ImagePath = opg.FileName;
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(ImagePath);
                bitmap.EndInit();
                testImage.Source = bitmap;
            }
        }

        private void processImage_Click(object sender, RoutedEventArgs e)
        {
            this.Input.ImageSource = this.ImagePath;

            var result = ConsumeModel.Predict(this.Input);
            this.txtResult.Text = $"It is: {result.Prediction} | ( Cat: {result.Score[0].ToString("P", CultureInfo.InvariantCulture)}" +
                $" | Dog: {result.Score[1].ToString("P", CultureInfo.InvariantCulture)} )";
        }
    }
}