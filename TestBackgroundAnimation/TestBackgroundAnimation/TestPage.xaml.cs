﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestBackgroundAnimation
{
    /// <summary>
    /// Interaction logic for TestPage.xaml
    /// </summary>
    public partial class TestPage : Page
    {
        private const int NumberOfColumns = 4;

        private const int NumberOfFrames = 4;

        private const int FrameWidth = 30;

        private const int FrameHeight = 30;

        public static readonly TimeSpan TimePerFrame = TimeSpan.FromSeconds(0.5);

        private int currentFrame;

        private TimeSpan timeTillNextFrame;

        public TestPage()
        {
            InitializeComponent();
        }

    }
}
