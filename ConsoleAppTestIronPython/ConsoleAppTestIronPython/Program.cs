﻿using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTestIronPython
{
    class Program
    {
        static void Main(string[] args)
        {
            int yNumber = 22;
            int xNumber = 10;

            ScriptEngine scriptEngine = Python.CreateEngine();
            ScriptScope scriptScope = scriptEngine.CreateScope();

            scriptScope.SetVariable("x", xNumber);
            scriptScope.SetVariable("y", yNumber);

            scriptEngine.ExecuteFile("hello.py", scriptScope);
            Console.ReadKey();
        }
    }
}
