﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewUIDesign.Model
{
    public class User
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public string Mail { get; set; }

        public SexType Sex { get; set; }

        public List<string> Versions { get; set; }

        public string Version { get; set; }

        public override string ToString()
        {
            return $"{Name} {Age}";
        }
    }
}
