﻿using NewUIDesign.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace NewUIDesign.UserControls
{

    public class MainWindowViewModel : INotifyPropertyChanged
    {

        //private CollectionViewSource usersCollection;
        private CollectionViewSource phrasesView;
        private string filter;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<User> Phrases { get; private set; }

        public MainWindowViewModel()
        {
            Phrases = new ObservableCollection<User>();

            Phrases.Add(new User() { Name = "John Doe", Age = 42, Version = "2020" });
            Phrases.Add(new User() { Name = "Jane Doe", Age = 39, Version = "2020" });
            Phrases.Add(new User() { Name = "Sammy Doe", Age = 13, Version = "2019" });
           // lvUsers.ItemsSource = items;

            phrasesView = new CollectionViewSource();
            phrasesView.Source = Phrases;
            phrasesView.Filter += PhrasesView_Filter;
        }

        public ICollectionView SourceCollection
        {
            get
            {
                return this.phrasesView.View;
            }
        }

        private void PhrasesView_Filter(object sender, FilterEventArgs e)
        {
            if (string.IsNullOrEmpty(Filter))
            {
                e.Accepted = true;
                return;
            }

            User usr = e.Item as User;
            if (usr.ToString().ToUpper().Contains(Filter.ToUpper()))
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        public string Filter
        {
            get
            {
                return filter;
            }
            set
            {
                if (value != filter)
                {
                    filter = value;
                    phrasesView.View.Refresh();
                    RaisePropertyChanged("Filter");
                }
            }
        }

        private void RaisePropertyChanged(string v)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("v"));
        }
    }
}
