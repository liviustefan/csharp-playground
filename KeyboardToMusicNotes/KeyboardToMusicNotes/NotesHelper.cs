﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyboardToMusicNotes
{
    public class NotesHelper
    {
        public Dictionary<string, string> WhiteKeyNotes { get; set; }

        public Dictionary<string, string> BlackKeyNotes { get; set; }

        public NotesHelper()
        {
            WhiteKeyNotes = new Dictionary<string, string>();
            BlackKeyNotes = new Dictionary<string, string>();

            this.BuildWhiteKeyNotesDictionary();
            this.BuildBlackKeyNotesDictionary();
        }

        private void BuildBlackKeyNotesDictionary()
        {
            //C1
            WhiteKeyNotes.Add("!", "#C1");
            WhiteKeyNotes.Add("@", "#D1");
            WhiteKeyNotes.Add("$", "#F1");
            WhiteKeyNotes.Add("%", "#G1");
            WhiteKeyNotes.Add("^", "#A1");
            //C2
            WhiteKeyNotes.Add("*", "#C2");
            WhiteKeyNotes.Add("(", "#D2");
            WhiteKeyNotes.Add("Q", "#F2");
            WhiteKeyNotes.Add("W", "#G2");
            WhiteKeyNotes.Add("E", "#A2");
            //C3
            WhiteKeyNotes.Add("T", "#C3");
            WhiteKeyNotes.Add("Y", "#D3");
            WhiteKeyNotes.Add("I", "#F3");
            WhiteKeyNotes.Add("O", "#G3");
            WhiteKeyNotes.Add("P", "#A3");
            //C4
            WhiteKeyNotes.Add("S", "#C4");
            WhiteKeyNotes.Add("D", "#D4");
            WhiteKeyNotes.Add("G", "#F4");
            WhiteKeyNotes.Add("H", "#G4");
            WhiteKeyNotes.Add("J", "#A4");
            //C5
            WhiteKeyNotes.Add("L", "#C5");
            WhiteKeyNotes.Add("Z", "#D5");
            WhiteKeyNotes.Add("C", "#F5");
            WhiteKeyNotes.Add("V", "#G5");
            WhiteKeyNotes.Add("B", "#A5");
        }

        private void BuildWhiteKeyNotesDictionary()
        {
            //C1
            WhiteKeyNotes.Add("1", "C1");
            WhiteKeyNotes.Add("2", "D1");
            WhiteKeyNotes.Add("3", "E1");
            WhiteKeyNotes.Add("4", "F1");
            WhiteKeyNotes.Add("5", "G1");
            WhiteKeyNotes.Add("6", "A1");
            WhiteKeyNotes.Add("7", "B1");
            //C2
            WhiteKeyNotes.Add("8", "C2");
            WhiteKeyNotes.Add("9", "D2");
            WhiteKeyNotes.Add("0", "E2");
            WhiteKeyNotes.Add("q", "F2");
            WhiteKeyNotes.Add("w", "G2");
            WhiteKeyNotes.Add("e", "A2");
            WhiteKeyNotes.Add("r", "B2");
            //C3
            WhiteKeyNotes.Add("t", "C3");
            WhiteKeyNotes.Add("y", "D3");
            WhiteKeyNotes.Add("u", "E3");
            WhiteKeyNotes.Add("i", "F3");
            WhiteKeyNotes.Add("o", "G3");
            WhiteKeyNotes.Add("p", "A3");
            WhiteKeyNotes.Add("a", "B3");
            //C4
            WhiteKeyNotes.Add("s", "C4");
            WhiteKeyNotes.Add("d", "D4");
            WhiteKeyNotes.Add("f", "E4");
            WhiteKeyNotes.Add("g", "F4");
            WhiteKeyNotes.Add("h", "G4");
            WhiteKeyNotes.Add("j", "A4");
            WhiteKeyNotes.Add("k", "B4");
            //C5
            WhiteKeyNotes.Add("l", "C5");
            WhiteKeyNotes.Add("z", "D5");
            WhiteKeyNotes.Add("x", "E5");
            WhiteKeyNotes.Add("c", "F5");
            WhiteKeyNotes.Add("v", "G5");
            WhiteKeyNotes.Add("b", "A5");
            WhiteKeyNotes.Add("n", "B5");
        }
    }
}
