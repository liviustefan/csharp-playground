﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/// <summary>
/// For online Piano
/// </summary>
namespace KeyboardToMusicNotes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NotesHelper notesHelper = new NotesHelper();

            List<string> finalArray = new List<string>();
            char[] letters = txtSource.Text.ToCharArray();
            
            for(int i = 0; i<letters.Count(); i++)
            {
                if(notesHelper.WhiteKeyNotes.ContainsKey(letters[i].ToString()))
                {
                    string note = notesHelper.WhiteKeyNotes[letters[i].ToString()];
                    finalArray.Add(note);
                }else if (notesHelper.BlackKeyNotes.ContainsKey(letters[i].ToString()))
                {
                    string note = notesHelper.BlackKeyNotes[letters[i].ToString()];
                    finalArray.Add(note);
                }
                else if(!string.IsNullOrWhiteSpace(letters[i].ToString()))
                {
                    finalArray.Add("\n");
                }
            }

            txtDisplay.Text = string.Join(" ", finalArray);
        }
    }
}
